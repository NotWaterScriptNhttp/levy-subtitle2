import datetime
import json

def converTime(time_var: str): # made by mr. Kiss
    found = False

    for s in time_var:
        if (s == ".") or (s == "s"):
            found = True

    if not found:
        time_var = time_var + ""

    print(time_var)

    num = float(time_var[:-1])
    res = datetime.timedelta(seconds=num)

    data = str(res).split(".")

    if str(data[0])[0:1] == "0":
        data[0] = "0"+data[0]

    if len(data) == 2:
        data[1] = str(data[1])[0:3]

    return f"{data[0]}{len(data) == 2 and '.'+data[1] or '.000'}"

def AddLine(lineNumber, line, start, end):
    wordConcated = ""

    for word in line:
        wordConcated += word + " "

    wordConcated = wordConcated[0:(len(wordConcated) - 1)]

    return f"{lineNumber}\r\n{converTime(start)} --> {converTime(end)}\r\n{wordConcated}\r\n\r\n"

def DoResult(results):
    sentenceCounter = 1

    characterCount = 0

    fileContents = ""
    words = []
    startTime = None
    endTime = None

    for res in results:
        current = res['alternatives']

        if current is None:
            print("result doesn't have any alternatives")
            exit(0)

        current = current[0]['words']

        if current is None:
            print("The JSON has no words")
            exit(0)

        for word in current:
            if word is None:
                print("word is not valid skipping")
                continue

            if startTime is None:
                startTime = word['startTime']

            words.append(word['word'])
            endTime = word['endTime']

            if (characterCount >= 40) or ((word['word'])[0:1].isupper()):
                #print(characterCount)
                fileContents = fileContents + AddLine(sentenceCounter, words, startTime, endTime)
                words.clear()
                characterCount = 0
                startTime = 0
                sentenceCounter += 1

            characterCount += len(word['word'])

    open("dialog.srt", "w").write(fileContents)


parsed_json = json.loads(open("text.json", "rb").read())

if parsed_json is None:
    print("Invalid json supplied!")
    exit(0)

if parsed_json['results'] is None:
    print("JSON has no valid results")
    exit(0)


DoResult(parsed_json['results'])